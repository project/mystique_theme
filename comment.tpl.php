<?php
/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */

?>
<div class="comment<?php print($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">
  <div class="clear-block">
    <div class="comment-head comment odd alt thread-odd thread-alt depth-1 withAvatars reader">
      <div class="avatar-box">
        <?php
          global $base_path;
          if ($picture) {
            print $picture;
          }
          else {
            print '<img width="48" height="48" src="'. $base_path .'sites/all/themes/mystique_theme/images/default.png" alt="photo" />';
          }
        ?>
      </div>
      <div class="author">
        <?php if ($submitted): ?>
          <span class="submitted"><?php print t($submitted); ?></span>
        <?php endif; ?>
        <?php if ($comment->new): ?>
          <span class="new"><?php print drupal_ucfirst($new) ?></span>
        <?php endif; ?>
        <?php if ($links): ?>
          <div class="links"><?php print $links ?></div>
        <?php endif; ?>
      </div>
    </div>
    <div class="comment-body clearfix">
      <h3><?php print $title ?></h3>
      <div class="content">
        <?php print t($content); ?>
        <?php if ($signature): ?>
          <div class="clear-block">
          <div>—</div>
          <?php print t($signature); ?>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
