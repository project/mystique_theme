<?php
?>
<?php
/**
 * Including custom stylesheets
 */
if (theme_get_setting('page_width') == '' || theme_get_setting('page_width') == 'fixed') {
  drupal_add_css(path_to_theme() .'/fixed.css', 'theme');
}
else {
  drupal_add_css(path_to_theme() .'/fluid.css', 'theme');
}
if (theme_get_setting('color_scheme') == '' ) {
  drupal_add_css(path_to_theme() .'/color-green.css', 'theme');
}
$color_scheme = theme_get_setting('color_scheme');
switch ($color_scheme) {
  case 'green':
    drupal_add_css(path_to_theme() .'/color-green.css', 'theme');
    break;
  case 'blue':
    drupal_add_css(path_to_theme() .'/color-blue.css', 'theme');
    break;
  case 'red':
    drupal_add_css(path_to_theme() .'/color-red.css', 'theme');
    break;
  case 'grey':
    drupal_add_css(path_to_theme() .'/color-grey.css', 'theme');
    break;
}

/**
 * Override of the Search Box
 * first, select the form ID
 */
function mystique_theme_theme() {
  return array(
    // The form ID.
    'search_theme_form' => array(
      // Forms always take the form argument.
      'arguments' => array(
        'form' => NULL,
      ),
    ),
  );
}

/**
 * Theme override for search form.
 */
function mystique_theme_search_theme_form($form) {
  $form['search_theme_form']['#title'] = NULL;
  $form['search_theme_form']['#value'] = t('Search');
  $form['search_theme_form']['#attributes'] = array(
    "class" => "searchFormBlock text clearField clearFieldBlurred",
  );
  $form['submit']['#value'] = '';
  $form['submit']['#attributes'] = array(
    "class" => "submit",
  );
  $output = '<div class="search-form">'. drupal_render($form) .'</div>';
  return $output;
}

function mystique_theme_preprocess_node(&$variables) {
  $node = $variables['node'];
  $variables['node']->num_comments = db_result(db_query("SELECT COUNT(*) FROM {comments} c WHERE c.nid = %d", $node->nid));
}

/**
 * Return a themed set of links.
 *
 * @param $links
 *   A keyed array of links to be themed.
 * @param $attributes
 *   A keyed array of attributes
 * @return
 *   A string containing an unordered list of links.
 */
function mystique_theme_links($links, $attributes = array('class' => 'links')) {
  global $language;
  $output = '';
  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';
    $num_links = count($links);
    $i = 1;
    foreach ($links as $key => $link) {
      //print $attributes['class'];
      $class = $key;
      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
      && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      if ($attributes['class']=="navigation") {
        $class .=" page ";
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';
      if (isset($link['href'])) {
        if ($attributes['class']=="navigation") {
          $output .='<a title="'. $link['title'] .'" href="'. url($link['href']) . '" class="fadeThis  sf-with-ul">
                      <span class="title">'. $link['title'] .'</span>
                      <span class="pointer"></span>
                      <span class="hover" style="opacity: 0;">
                      </span>
                  </a>';
          $menu_array_space = explode(" ", $key);
          $menu_array = explode("-", $menu_array_space[0]);
          $menuload=menu_link_load($menu_array[1]);
          if (!empty($menuload['has_children'])) {
              $output .=load_all_sub_menu($menuload);
          }
        }
        else {
          // Pass in $link as $options, they share the same keys.
          $output .= l($link['title'], $link['href'], $link);
        }
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }
      $i++;
      $output .= "</li>\n";
    }
    $output .= '</ul>';
  }
  return $output;
}
function _menu_subtree_data($item, $tree) {
  foreach ($tree as $cid => $data) {
    if ($item['mlid'] == $data['link']['mlid']) {
      return array($cid => $data);
    }
    if ($data['below']) {
      $return = _menu_subtree_data($item, $data['below']);
      if ($return) {
        return $return;
      }
    }
  }
  return FALSE;
}

function mystique_theme_menu_local_tasks() {
  $output = '';
  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs\">\n". $primary ."</ul>\n";
  }
  /*
  if ($secondary = menu_secondary_local_tasks()) {
  $output .= "<ol class=\"tabs secondary\">\n". $secondary ."</ol>\n";
  }
  */
  return $output;
}

function load_all_sub_menu($menuload, $output=NULL) {
  $output .='<ul class="level-2" style="opacity: 0; margin-left: 20px; display: none; visibility: visible;">';
  $tree = _menu_subtree_data($menuload, menu_tree_all_data($menuload['menu_name']));
  foreach ($tree as $menu) {
    foreach ($menu['below'] as $submenu) {
      $output .= '<li class="page page-'. $submenu['link']['title'] .'" >';
      $output .= '<a title="'. $submenu['link']['title'] .'" href="'. url($submenu['link']['href']) . '" class="fadeThis">
                  <span class="title">'. $submenu['link']['title'] .'</span>
                  <span class="pointer"></span>
                  <span class="hover" style="opacity: 0;">
                  </span>
                  </a>';
      if ($submenu['below']) {
        $menuload=menu_link_load($submenu['link']['mlid']);
        $output .=load_all_sub_menu($menuload);
      }
      $output .='</li>';
    }
  }
  $output .='</ul>';
  return $output;
}

function mystique_theme_comment_submitted($comment) {
  return t('Posted by !username on @date at about @time.',
    array(
      '!username' => theme('username', $comment),
      '@date' => format_date($comment->timestamp, 'custom', 'd M Y'),
      '@time' => format_date($comment->timestamp, 'custom', 'H:i')
    )
  );
}

/**
 * Implementation of mytheme_page().
 */
function mystique_theme_preprocess_page(&$variables) {
  $variables['body_classes'] .= ' blog ';
  $right = (isset($variables['right'])) ? $variables['right'] : '';
  $left  = (isset($variables['left'])) ? $variables['left'] : ''; 
  if ($variables['is_front']) {
    $variables['body_classes'] .= 'home ';
  }
  if (theme_get_setting('page_width') == '') {
    $width_class = 'fixed ';
  }
  else {
    $width_class = theme_get_setting('page_width').' ';
  }
  $variables['body_classes'] .= $width_class;
  if (theme_get_setting('page_layout') == '') {
    $page_layout = 'three_col';
  }
  else {
    $page_layout = theme_get_setting('page_layout');
  }
  switch ($page_layout) {
    case 'one_col':
      $layout_class = 'col-1 ';
      break;
    case 'two_col_right':
      if (!$right){
      	$layout_class = 'col-1 ';
      }
      else{
        $layout_class = 'col-2-right ';
      }
      break;
    case 'three_col':
      if ($right && !$left){
      	$layout_class = 'col-2-right ';
      }	
      elseif($left && !$right) {
      	$layout_class = 'col-2-left ';
      }
      elseif($left && $right) {
        $layout_class = 'col-3 ';
      }
      elseif(!$right && !$left) {
      	$layout_class = 'col-1 ';
      }	
      break;
  }
  $variables['body_classes'] .= $layout_class;
}
