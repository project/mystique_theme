<?php
?>
<?php
/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
 */
function phptemplate_settings($saved_settings) {
  $defaults = array(
    'color_scheme' => 'green',
    'font_style' => 'arial',
    'page_width' => 'fixed',
    'page_layout' => 'three_col',
    'twitter_user' => 'drupal',
  );
  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);
  $form['color_scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Color scheme'),
    '#default_value' => $settings['color_scheme'],
    '#options' => array(
      'green' => t('Green'),
      'blue' => t('Blue'),
      'red' => t('Red'),
      'grey' => t('Grey')
    ),
  );
  $form['font_style'] = array(
    '#type' => 'radios',
    '#title' => t('Font style'),
    '#default_value' => $settings['font_style'],
    '#options' => array(
      'segoe' => t('Segoe UI (Windows Vista/7)'),
      'arial' => t('Helvetica/Arial'),
      'georgia' => t('Georgia (sans serif)'),
      'lucida' => t('Lucida Grande/Sans (Mac/Windows)')
    ),
  );
  $form['page_width'] = array(
    '#type' => 'radios',
    '#title' => t('Page Width'),
    '#default_value' => $settings['page_width'],
    '#options' => array(
      'fixed' => t('Fixed (960gs)'),
      'fluid' => t('Fluid (100%)'),
    ),
  );
  $form['page_layout'] = array(
    '#type' => 'radios',
    '#title' => t('Page Layout'),
    '#default_value' => $settings['page_layout'],
    '#options' => array(
      'one_col' => t('One Column'),
      'two_col_right' => t('Two Columns (Main + Right Sidebar)'),
      'three_col' => t('Three Columns (Left Sidebar + Main + Right Sidebar)'),
    ),
  );
  $form['twitter_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter user'),
    '#description' => t('The twitter username to which the twitter icon is to be linked.'),
    '#default_value' => $settings['twitter_user'],
  );
  return $form;
}

