<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head profile="http://gmpg.org/xfn/11">
    <?php print $head ?>
    <title><?php print t($head_title); ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <?php
      global $base_path;
    ?>
    <style type="text/css" >
      <?php
      $font_family = theme_get_setting('font_style');
      switch ($font_family) {
        case 'segoe':
          print '*{font-family:"Segoe UI",Calibri,"Myriad Pro",Myriad,"Trebuchet MS",Helvetica,Arial,sans-serif}';
          break;
        case 'arial':
          print '*{font-family:"Helvetica Neue",Helvetica,Arial,Geneva,"MS Sans Serif",sans-serif}';
          break;
        case 'georgia':
          print '*{font-family:Georgia,"Nimbus Roman No9 L",serif}';
          break;
        case 'lucida':
          print '*{font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode","Helvetica Neue",Helvetica,Arial,Verdana,sans-serif}';
          break;
      }
      ?>
    </style>
    <!--[if lte IE 6]><link media="screen" rel="stylesheet" href="<?php echo $base_path; ?>sites/all/themes/mystique_theme/ie6.css" type="text/css" /><![endif]-->
    <!--[if IE 7]><link media="screen" rel="stylesheet" href="<?php echo $base_path; ?>sites/all/themes/mystique_theme/ie7.css" type="text/css" /><![endif]-->
  </head>
  <body class="<?php print $body_classes?> browser-gecko">
    <div id="page">
      <div class="page-content header-wrapper">
        <div id="header" class="bubbleTrigger">
          <div id="site-title" class="clearfix">
            <?php
            global $base_path;
            print '<h1 id="logo"><a href="'. $base_path .'">';
            print t($site_name);
            print '</a></h1>';
            if ($site_slogan) {
              print '<p class="headline">'. $site_slogan .'</p>';
            }
            if ($logo) {
              print '<div class="logo"><a href="'. $base_path .'"><img src="'. check_url($logo) .'" alt="'. $site_name .'" /></a></div>';
            }
            ?>
          </div>
          <div class="shadow-left">
            <div class="shadow-right clearfix">
              <p class="nav-extra">
                <a href="<?php print url('rss.xml') ?>"
                   class="nav-extra rss" title="RSS Feeds"
                ><span><?php print t('RSS Feeds'); ?></span></a>
                <a href="http://www.twitter.com/<?php print theme_get_setting('twitter_user') ?>"
                   class="nav-extra twitter" title="<?php print t('Follow me on Twitter!');?>"
                ><span><?php print t('Follow me on Twitter!');?></span></a>
              </p>
              <?php
              if ($primary_links) {
                print theme('links', $primary_links, array('class' => 'navigation'));
              }
              else {
                print '
                  <ul class="navigation">
                    <li>
                      <a class="fadeThis" href="'. $base_path .'">
                        <span class="title">'. t('Home') .'</span>
                        <span class="pointer"></span>
                        <span style="opacity: 0;" class="hover">
                        </span>
                      </a>
                    </li>
                  </ul>';
              }
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="shadow-left page-content main-wrapper">
        <div class="shadow-right">
          <div id="main">
            <div id="main-inside" class="clearfix">
               <div id ="content-top">
                 <?php if ($content_top): print $content_top; endif; ?>
               </div>
              <div id="primary-content">
                <div class="blocks">
                  <div class="single-navigation clearfix">
                    <div class="alignleft">
                      <?php print $breadcrumb; ?>
                    </div>
                    <div class="alignright"></div>
                  </div>
                  <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
                  <?php if ($show_messages && $messages): print $messages; endif; ?>
                  <?php if ($tabs): print '<div id="tabs-wrapper" class="tabs-wrap clear-block">'; endif; ?>
                  <?php if ($tabs): print $tabs . '</div>'; endif; ?>
                  <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>                  
                  <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
                  <?php print $help; ?>                  
                  <?php print $content ?>
                </div>
              </div>
              <?php if ($left): ?>
                <div id="sidebar2">
                  <div class="blocks">
                    <?php print $left ?>
                  </div>
                </div>
              <?php endif ;?>
              <?php if ($right): ?>
                <div id="sidebar">
                  <div class="blocks">
                    <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
                    <?php print $right ?>
                  </div>
                </div>
              <?php endif ;?>              
            </div>
          </div>
          <div id="footer">
            <div class="page-content">
              <div id="copyright">
                <div class="footer_content">
                  <?php print $footer; ?>
                </div>
                <div class="attribution">
                  Design by <a href="http://digitalnature.ro">digitalnature</a>
                  | Theme by <a href="http://www.zyxware.com">Zyxware</a>
                </div>
                <div class="footer_icon">
                  <!--<div class="style_rss_feed">-->
                  <a title="RSS Feeds" href="<?php print url('rss.xml'); ?>"
                    class="rss-subscribe"
                  ><?php print t('RSS Feeds'); ?></a>
                  <!--</div>
                  <div class="style_xml">-->
                  <a title="Valid XHTML"
                     href="http://validator.w3.org/check?uri=referer" class="valid-xhtml"
                  >XHTML 1.1</a>
                  <!--</div>
                  <div class="style_gotop">-->
                  <a class="js-link" id="goTop" href="#page"
                  ><?php print t('Top'); ?></a>
                  <!--</div>-->
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <div id="pageControls">
      </div>
    </div>
    <?php print $closure ?>
  </body>
</html>
