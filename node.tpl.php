<?php
?>
<div id="node-<?php print $node->nid; ?>"
     class="page node<?php if ($sticky) { print ' sticky'; }?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <?php print $picture ?>
  <?php if ($page == 0): ?>
  <div class="node-title">
    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </div>
  <?php endif; ?>

  <?php if ($submitted): ?>
    <div class="post-date">
      <p class="day"><?php print format_date($node->created, 'custom', 'D, j M, Y' );?></p>
    </div>
    <div class="post-info clearfix ">
      <p class="author alignleft"><?php print t('Posted by !user at @time', array('!user' => theme('username', $node), '@time' => format_date($node->created, 'custom', 'H:i')));?></p>
      <?php if ($comment_count > 0): ?>
        <p class="comments alignright"><a class=" comments" href="<?php print $node_url ?>#comments"><?php print format_plural($comment_count, "@count".t(comment), "@count ".t(comments), array('@count' => $comment_count)); ?></a></p>
      <?php else: ?>
        <p class="comments alignright"><a class="no comments" href="<?php print $node_url ?>#comments"><?php print t('No comments')?></a></p>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <div class="content clear-block">
    <?php print t($content) ?>
  </div>

  <div class="clear-block">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="post-tags"><?php print $terms ?></div>
    <?php endif;?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>
</div>
