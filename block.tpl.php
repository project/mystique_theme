<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="arbitrary-block clear-block block block-<?php print $block->module ?>">
  <?php if (!empty($block->subject)): ?>
    <h3 class="title"><span><?php print $block->subject ?></span></h3>
    <div class="block-div"></div>
    <div class="block-div-arrow"></div>
  <?php endif;?>
  <div class="content"><?php print $block->content ?></div>
</div>
